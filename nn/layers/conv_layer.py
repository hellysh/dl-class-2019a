from typing import Optional, Callable
import numpy as np

from numba import njit, prange

from nn import Parameter
from .layer import Layer


class ConvLayer(Layer):
    def __init__(self, input_channels, output_channels, kernel_size=3, stride=1, parent=None):
        super(ConvLayer, self).__init__(parent)
        self.weight = Parameter(np.zeros((input_channels, output_channels, kernel_size, kernel_size), dtype=np.float32))
        self.bias = Parameter(np.zeros(output_channels, dtype=np.float32))
        self.kernel_size = kernel_size
        self.padding = (kernel_size - 1) // 2
        self.stride = stride
        self.cache = None
        self.initialize()


    @staticmethod
    @njit(parallel=True, cache=True)
    def forward_numba(data, padded_data, weight, bias, padding, stride):
        batch, input_channels, height, width = data.shape
        input_channels, output_channels, kernel_size, _ = weight.shape

        out_height = (height - kernel_size + 2*padding) // stride + 1
        out_width = (width - kernel_size + 2*padding) // stride + 1
        out = np.zeros((batch, output_channels, out_height, out_width))

        for batch_idx in prange(batch):
            for out_ch in prange(output_channels):
                for in_ch in prange(input_channels):
                    for h in prange(out_height):
                        for w in prange(out_width):
                            out[batch_idx, out_ch, h, w] += bias[out_ch]
                            h_i = h * stride
                            w_i = w * stride
                            total = 0
                            for i in prange(kernel_size):
                                for j in prange(kernel_size):
                                    total += weight[in_ch, out_ch, i, j] * padded_data[batch_idx, in_ch, h_i+i, w_i+j]
                            out[batch_idx, out_ch, h, w] += total


        return out

    @staticmethod
    @njit(parallel=True, cache=True)
    def backward_numba(data, padded_data, weight, bias, ppg, weight_grad, bias_grad, padding, stride):
        batch, input_channels, height, width = data.shape
        input_channels, output_channels, kernel_size, _ = weight.shape
        _, _, out_height, out_width = ppg.shape

        padded_out = np.zeros((batch, input_channels, height + padding * 2, width + padding * 2))

        for batch_idx in range(batch):
            for out_ch in range(output_channels):
                for h in range(out_height):
                    for w in range(out_width):
                        bias_grad[out_ch] += ppg[batch_idx, out_ch, h, w]
                        for in_ch in range(input_channels):
                            h_i = h * stride
                            w_i = w * stride
                            for i in range(kernel_size):
                                for j in range(kernel_size):
                                    weight_grad[in_ch, out_ch, i, j] += ppg[batch_idx, out_ch, h, w] * padded_data[batch_idx, in_ch, h_i + i, w_i + j]
                                    padded_out[batch_idx, in_ch, h_i + i, w_i + j] += ppg[batch_idx, out_ch, h, w] * weight[in_ch, out_ch, i, j]

        return padded_out[:, :, padding:height + padding, padding:width + padding]

    def forward(self, data):
        self.data = data
        batch, input_channels, height, width = data.shape

        padded_data = np.zeros((batch, input_channels, height + self.padding * 2, width + self.padding * 2))
        padded_data[:, :, self.padding:height + self.padding, self.padding:width + self.padding] = data
        self.padded_data = padded_data

        result = self.forward_numba(data, padded_data, self.weight.data, self.bias.data, self.padding, self.stride)
        return result

    def backward(self, previous_partial_gradient):
        result = self.backward_numba(self.data, self.padded_data, self.weight.data, self.bias.data, previous_partial_gradient, self.weight.grad, self.bias.grad, self.padding, self.stride)
        return result

    def selfstr(self):
        return "Kernel: (%s, %s) In Channels %s Out Channels %s Stride %s" % (
            self.weight.data.shape[2],
            self.weight.data.shape[3],
            self.weight.data.shape[0],
            self.weight.data.shape[1],
            self.stride,
        )

    def initialize(self, initializer: Optional[Callable[[Parameter], None]] = None):
        if initializer is None:
            self.weight.data = np.random.normal(0, 0.1, self.weight.data.shape)
            self.bias.data = 0
        else:
            for param in self.own_parameters():
                initializer(param)
        super(ConvLayer, self).initialize()

'''
    @staticmethod
    def im2col(im, kernel_size, stride):
        batch, input_channels, height, width = im.shape
        out_height = int((height - kernel_size) / stride + 1)
        out_width = int((width - kernel_size) / stride + 1)
        col = np.zeros((input_channels * kernel_size * kernel_size, out_height * out_width * batch))

        c = 0
        for b in range(batch):
            for h in range(out_height):
                for w in range(out_width):
                    for i in range(input_channels):
                        h_i = h * stride
                        w_i = w * stride
                        index = i * kernel_size * kernel_size
                        col[index:(index + kernel_size * kernel_size), c] = im[b, i, h_i:(h_i + kernel_size), w_i:(w_i + kernel_size)].reshape(kernel_size * kernel_size)
                    c += 1

        return col

    @staticmethod
    def col2im(col, data_size, kernel_size, stride):
        batch, input_channels, height, width = data_size
        im = np.zeros(data_size)
        weights = np.zeros(data_size)

        out_height = int((height - kernel_size) / stride + 1)
        out_width = int((width - kernel_size) / stride + 1)

        c = 0
        for b in range(batch):
            for h in range(out_height):
                for w in range(out_width):
                    for i in range(input_channels):
                        index = i * kernel_size * kernel_size
                        im[b, i, h:h + kernel_size, w:w + kernel_size] += col[index:index + (kernel_size * kernel_size), c].reshape((kernel_size, kernel_size))
                        weights[b, i, h:h + kernel_size, w:w + kernel_size] += np.ones((kernel_size, kernel_size))
                    c += 1

        return im / weights
    '''