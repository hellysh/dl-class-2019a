import numbers

import numpy as np
from numba import njit, prange

from .layer import Layer


class MaxPoolLayer(Layer):
    def __init__(self, kernel_size: int = 2, stride: int = 2, parent=None):
        super(MaxPoolLayer, self).__init__(parent)
        self.kernel_size = kernel_size
        self.padding = (kernel_size - 1) // 2
        self.stride = stride

    @staticmethod
    @njit(parallel=True, cache=True, fastmath=True)
    def forward_numba(data, padding, stride, kernel_size, max_indices):
        batch, input_channels, height, width = data.shape

        padded_data = np.zeros((batch, input_channels, height + padding * 2, width + padding * 2))
        padded_data[:, :, padding:height + padding, padding:width + padding] = data

        out_height = (height - kernel_size + 2*padding) // stride + 1
        out_width = (width - kernel_size + 2*padding) // stride + 1
        out = np.zeros((batch, input_channels, out_height, out_width))

        for batch_idx in prange(batch):
            for in_ch in prange(input_channels):
                for h in prange(out_height):
                    for w in range(out_width):
                        h_i = h * stride
                        w_i = w * stride
                        curr_max = 0
                        for i in range(kernel_size):
                            for j in range(kernel_size):
                                curr_value = padded_data[batch_idx, in_ch, h_i+i, w_i+j]
                                if curr_value > curr_max:
                                    curr_max = curr_value
                                    max_indices[batch_idx, in_ch, h, w][0] = i
                                    max_indices[batch_idx, in_ch, h, w][1] = j
                        
                        out[batch_idx, in_ch, h, w] = curr_max

        return out

    @staticmethod
    @njit(parallel=True, cache=True, fastmath=True)
    def backward_numba(data, padded_data, ppg, padding, stride, kernel_size, max_indices):
        batch, input_channels, height, width = data.shape
        _, _, out_height, out_width = ppg.shape

        padded_out = np.zeros((batch, input_channels, height + padding * 2, width + padding * 2))

        for batch_idx in prange(batch):
            for in_ch in prange(input_channels):
                for h in prange(out_height):
                    for w in range(out_width):
                        h_i = h * stride
                        w_i = w * stride
                        # we only want to prop the cell that affected the output - eg the max cell
                        max_i = max_indices[batch_idx, in_ch, h, w][0]
                        max_j = max_indices[batch_idx, in_ch, h, w][1]
                        padded_out[batch_idx, in_ch, h_i+max_i, w_i+max_j] += ppg[batch_idx, in_ch, h, w]
        
        return padded_out[:, :, padding:height + padding, padding:width + padding]

    def forward(self, data):
        self.data = data
        batch, input_channels, height, width = data.shape

        padded_data = np.zeros((batch, input_channels, height + self.padding * 2, width + self.padding * 2))
        padded_data[:, :, self.padding:height + self.padding, self.padding:width + self.padding] = data
        self.padded_data = padded_data

        # stores which cell contains the max - used for back prop
        self.max_indices = np.zeros((batch, input_channels, height, width, 2)).astype(int)

        result = self.forward_numba(data, self.padding, self.stride, self.kernel_size, self.max_indices)
        return result

    def backward(self, previous_partial_gradient):
        result = self.backward_numba(self.data, self.padded_data, previous_partial_gradient, self.padding, self.stride, self.kernel_size, self.max_indices)
        return result

    def selfstr(self):
        return str("kernel: " + str(self.kernel_size) + " stride: " + str(self.stride))
