import numpy as np
from numba import njit, prange

from .layer import Layer


class LeakyReLULayer(Layer):
    def __init__(self, slope: float = 0.1, parent=None):
        super(LeakyReLULayer, self).__init__(parent)
        self.slope = slope

    def forward(self, data):
        # TODO
        self.data = data
        output = self.forward_lrelu(data, self.slope)
        return output

    def backward(self, previous_partial_gradient):
        # TODO
        output = self.backward_lrelu(self.data, self.slope, previous_partial_gradient)
        return output

    @staticmethod
    def forward_lrelu(data, slope):
        return np.where(data > 0, data, data * slope)

    @staticmethod
    def backward_lrelu(data, slope, grad):
        return np.where(data > 0, grad, grad * slope)
