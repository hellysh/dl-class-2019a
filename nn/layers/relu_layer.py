import numpy as np
from numba import njit, prange

from .layer import Layer


class ReLULayer(Layer):
    def __init__(self, parent=None):
        super(ReLULayer, self).__init__(parent)
        self.data = None

    def forward(self, data):
        # TODO
        self.data = data
        output = self.forward_relu(data)
        return output

    def backward(self, previous_partial_gradient):
        # TODO
        output = self.backward_relu(self.data, previous_partial_gradient)
        return output

    @staticmethod
    def forward_relu(data):
        return np.maximum(0, data)        
    
    @staticmethod
    def backward_relu(data, grad):
        return np.where(data > 0, grad, 0)

class ReLUNumbaLayer(Layer):
    def __init__(self, parent=None):
        super(ReLUNumbaLayer, self).__init__(parent)
        self.data = None

    @staticmethod
    @njit(parallel=True, cache=True)
    def forward_numba(data):
        # TODO Helper function for computing ReLU
        flattened_data = np.array(data).flatten()
        for i in flattened_data:
            flattened_data[i] = max(0, flattened_data[i])
        return flattened_data.reshape(data.shape)

    def forward(self, data):
        # TODO
        self.data = data
        output = self.forward_numba(data)
        return output

    @staticmethod
    @njit(parallel=True, cache=True)
    def backward_numba(data, grad):
        # TODO Helper function for computing ReLU gradients
        flattened_data = np.array(data).flatten()
        flattened_grad = np.array(grad).flatten()
        for i in flattened_data:
            if flattened_data[i] <= 0:
                flattened_grad[i] = 0
        return flattened_grad

    def backward(self, previous_partial_gradient):
        # TODO
        self.backward_numba(self.data, previous_partial_gradient)
        return None
