from .layer import Layer
import numpy as np


class FlattenLayer(Layer):
    def __init__(self, parent=None):
        super(FlattenLayer, self).__init__(parent)
        self.old_data_shape = None

    def forward(self, data):
        # TODO reshape the data here and return it (this can be in place).
        output = np.array(data)
        output = output.reshape((data.shape[0], -1))

        self.old_data_shape = data.shape
        return output

    def backward(self, previous_partial_gradient):
        # TODO
        output = np.array(previous_partial_gradient)
        output = output.reshape(self.old_data_shape)

        return output
