import numpy as np
from numba import njit, prange

from nn import Parameter
from .layer import Layer


class PReLULayer(Layer):
    def __init__(self, size: int, initial_slope: float = 0.01, parent=None):
        super(PReLULayer, self).__init__(parent)
        self.slope = Parameter(np.full(size, initial_slope))
        self.size = size

    def forward(self, data):
        # TODO
        self.data = data
        output = self.forward_prelu(data, self.slope, self.size)
        return output

    def backward(self, previous_partial_gradient):
        # TODO
        output = self.backward_prelu(self.data, self.slope, self.size, previous_partial_gradient)
        return output

    @staticmethod
    def forward_prelu(data, slope, size):
        if (size == 1):
            return np.where(data > 0, data, data * slope.data[0])
        else:
            reshaped_slope = np.repeat(slope.data[:, np.newaxis], data.shape[0], axis=1)
            reshaped_slope = np.repeat(reshaped_slope.T[:, :, np.newaxis], data.shape[2], axis=2)
            return np.where(data > 0, data, data * reshaped_slope)

    @staticmethod
    def backward_prelu(data, slope, size, grad):
        #compress data down to a single axis (axis 1)
        compressed_data = data
        num_axes = len(data.shape)
        if num_axes > 1:
            # we compress down the first axis, then we can keep compressing down
            # any other axis until we get down to a single dimension. Note we
            # keep summing over "axis=1" because the axes shrink with each iteration
            compressed_data = np.mean(compressed_data, axis=0)
            for i in range(2, num_axes):
                compressed_data = np.mean(compressed_data, axis=1)

        if (size == 1):
            compressed_data = np.mean(compressed_data, axis=0)
            slope.grad = np.where(compressed_data > 0, 0, compressed_data)
            return np.where(data > 0, grad, grad * slope.data[0])
        else:
            slope.grad = np.where(compressed_data > 0, 0, compressed_data)
            reshaped_slope = np.repeat(slope.data[:, np.newaxis], data.shape[0], axis=1)
            reshaped_slope = np.repeat(reshaped_slope.T[:, :, np.newaxis], data.shape[2], axis=2)
            return np.where(data > 0, grad, grad * reshaped_slope)